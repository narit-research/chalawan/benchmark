# !/bin/bash

F_LOG="TeslaV100SXM2_v1.1/log1/BM_TF_ANb512_1gpu.log"

echo "model, variable, device, batch, optimizer, no_gpu, r1, r2, r3, r4, r5"
col=$(awk -v gpu=1 'BEGIN {FS=" "; OFS=","; ORS="\n"}
     NR==3 {model=$2}
     NR==6 {batch=$1}
     NNR==9 {optim=$2}
     NR==10 {variable=$2}
     NR==30 {result=$3}
     END {print model,variable,"gpu",batch, gpu, result}
    ' $F_LOG)
echo "-----"
echo $col

col2=$(awk '$1~"total" {print $3}' $F_LOG)
echo $col2
