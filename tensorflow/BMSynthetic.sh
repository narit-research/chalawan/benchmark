# !/bin/bash

echo "Start benchmarking TensorFlow, first select TensorFlow version: "
select version in "1.1" "1.9"
do
	case $version in "1.1")
		echo "version $version selected."
		# remove all modules and load
		module purge ; module load cuda/8.0
		# activate python virtualenv
		source ~/python/tf1.1.0rc2/bin/activate
		break;;
	"1.9")
		echo "version $version selected."
		# remove all modules and load
		module purge ; module load cuda/9.1
		# activate python virtualenv
		source ~/python/tf1.9/bin/activate
		break;;
	esac
	export $version
done

# define benchmark source folder
BM_DIR="/home/ittipat/benchmark/tensorflow_src/scripts/tf_cnn_benchmarks"
BM_RUN="python ${BM_DIR}/tf_cnn_benchmarks.py"
# search for card model
gpu_model=$(lspci | grep -i nvidia | awk 'NR==1 {print $7,$8,$9}')
# define output log folder and create new one if doesn't exit
OUT_DIR="/home/ittipat/benchmark/tensorflow/$(echo $gpu_model | tr --delete "[ ]")_v${version}"
if [ ! -d "$OUT_DIR" ]; then
    mkdir -p $OUT_DIR
fi
# define log name prefix. The naming custom is <instance_type>_<program_name>
## For instance type: BM (Bare Metal machine) and DK (Docker machine).
## TensorFlow --> TF
LOG_NAME_PREFIX="BM_TF"

# declare an array of model and get the number of model
declare -a model=("inception3" "resnet50" "resnet152" "alexnet" "vgg16")
declare -i num_model=${#model[@]}
# declare model's name abbrv.
declare -a MD=("IC3" "R50" "R152" "AN" "VGG")
# declare variable_update
declare -a var_up=("parameter_server" "replicated" "replicated" "parameter_server" "parameter_server")
# declare an array of batch size
declare -a batch_size=(64 64 32 512 32)
# declare number of gpu
declare -a num_gpus=(1 2 4)

# do the benchmarking continuously 5 times
for i in {1..5};
do  echo "The following model will be used for benchmark..."
    LOG_DIR="${OUT_DIR}/log${i}"
    if [ ! -d "$LOG_SIR" ]; then
        mkdir -p $LOG_DIR
    fi
    for (( item=0; item<${num_model}; item++ ));
    do
        echo "Start running model ${model[$item]} with synthetic data (${i}/5)";
        echo "Batch size: ${batch_size[$item]}";
        for (( gpu=1; gpu<8; gpu*=2 ));
        do
            echo "Number of GPUs: $gpu";
            LOG_FILE="${LOG_DIR}/${LOG_NAME_PREFIX}_${MD[$item]}b${batch_size[$item]}_${gpu}gpu.log";
            if [ -f $LOG_FILE ]; then
                continue;
            else
                touch $LOG_FILE;
            fi;
	    echo "GPU model: ${gpu_model}" > $LOG_FILE;
            $BM_RUN --num_gpus=${gpu} --batch_size=${batch_size[$item]} --model=${model[$item]} --variable_update=${var_up[$item]} >> $LOG_FILE;
            #wait 10;
        done
    done
done

deactivate
