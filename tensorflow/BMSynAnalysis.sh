# !/bin/bash

select version in "1.1" "1.9"
do
        case $version in "1.1")
                break;;
        "1.9")
                break;;
        esac
        echo "version $version selected."
        export $version
done


# search for card model
gpu_model=$(lspci | grep -i nvidia | awk 'NR==1 {print $7,$8,$9}')
# define output log folder and create new one if doesn't exit
OUT_DIR="/home/ittipat/benchmark/tensorflow/$(echo $gpu_model | tr --delete "[ ]")_v${version}"
if [ ! -d "$OUT_DIR" ]; then
    mkdir -p $OUT_DIR
fi
# define analized file
F_AZ="/home/ittipat/benchmark/tensorflow/$(echo $gpu_model | tr --delete "[ ]")_v${version}.log"
if [ -f $F_AZ ]; then
    rm $F_AZ
fi
touch $F_AZ 
echo "model, variable, device, batch, optimizer, no_gpu, r1, r2, r3, r4, r5" > $F_AZ 
# define log name prefix. The naming custom is <instance_type>_<program_name>
## For instance type: BM (Bare Metal machine) and DK (Docker machine).
## TensorFlow --> TF
LOG_NAME_PREFIX="BM_TF"

# declare an array of model and get the number of model
declare -a model=("inception3" "resnet50" "resnet152" "alexnet" "vgg16")
declare -i num_model=${#model[@]}
echo "num_model=$num_model"
# declare model's name abbrv.
declare -a MD=("IC3" "R50" "R152" "AN" "VGG")
# declare variable_update
declare -a var_up=("parameter_server" "replicated" "replicated" "parameter_server" "parameter_server")
# declare an array of batch size
declare -a batch_size=(64 64 32 512 32)
# declare number of gpu
declare -a num_gpus=(1 2 4)
# declare result array
declare -a result=()

# define log files directories
for (( item=0; item<${num_model}; item++ )); do
    for (( gpu=1; gpu<8; gpu*=2 )); do
        for i in {1..5}; do
            LOG_DIR="${OUT_DIR}/log${i}"
            LOG_FILE="${LOG_DIR}/${LOG_NAME_PREFIX}_${MD[$item]}b${batch_size[$item]}_${gpu}gpu.log";
            #echo $LOG_FILE
            if [ $i -eq 1 ]; then
		if [ "$version" == "1.1" ]; then
                    result[$i]=$(awk -v gpu=$gpu 'BEGIN {FS=" "; OFS=","; ORS="\n"}
                    NR==3 {model=$2}
                    NR==6 {batch=$1}
                    NR==9 {optim=$2}
                    NR==10 {variable=$2}
                    $1~"total" {result=$3}
                    END {print model,variable,"gpu",batch,optim,gpu,result}
                    '  $LOG_FILE);
                elif [ "$version" == "1.9" ]; then
                    result[$i]=$(awk -v gpu=$gpu 'BEGIN {FS=" "; OFS=","; ORS="\n"}
                    NR==3 {model=$2}
                    NR==8 {batch=$1}
                    NR==14 {optim=$2}
                    NR==15 {variable=$2}
                    $1~"total" {result=$3}
                    END {print model,variable,"gpu",batch,optim,gpu,result}
                    '  $LOG_FILE);
                fi
            else
                result[$i]=$(awk 'BEGIN {FS=" "; OFS=","}
                $1~"total" {print $3}' $LOG_FILE);
            fi
	done
        echo "${result[1]},${result[2]},${result[3]},${result[4]},${result[5]}" >> $F_AZ
    done
done

