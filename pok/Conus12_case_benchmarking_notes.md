﻿# Benchmarking WRF with Conus cases

We'll use the benchmark case from http://www2.mmm.ucar.edu/wrf/WG2/bench/

## Preparation

1.Compile WRF and WPS

* Compile WRF and WPS, refer to the documentaion below for more details
* Official UCAR tutorial [Link](http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compilation_tutorial.php)
* Instruction for Intel Compilers [Link](https://software.intel.com/en-us/articles/wrf-and-wps-v311-installation-bkm-with-inter-compilers-and-intelr-mpi)
* User Guide.  [Link](http://www2.mmm.ucar.edu/wrf/users/docs/user_guide_V3/contents.html) See [2.  Software Installation](http://www2.mmm.ucar.edu/wrf/users/docs/user_guide_V3.8/users_guide_chap2.htm)

2.Download benchmark data

* **CASE 1**
* [Data download link](http://www2.mmm.ucar.edu/WG2bench/conus12km_data_v3/)
* Download all files in to a folder, this will be referred as `$case1_data`
* Edit `namelist.input` file by adding `use_baseparam_fr_nml = .true.` at LINE 86 (or anywhrer between `&dynamics` and `non_hydrostatic = .true.,`)

* **CASE 2**
* [Data download link](http://www2.mmm.ucar.edu/WG2bench/conus_2.5_v3/)
* Follow instruction from http://www2.mmm.ucar.edu/WG2bench/conus_2.5_v3/READ-ME.txt
* Download all files in to a folder, this will be referred as `$case2_data`

3.Unlimit stack size

* Find equivalent command in your other system.

```bash
ulimit -s unlimited
```

## Instruction for running WRF

Direction for the run differs slightly for each CASE, which I'll guide through case-by-case.

### CASE 1

1) Make sure that WRF compile successfully by checking the contents of directory `WRFV3/run`

```bash
ls /lustre/ARU/aru/opt/WRF3.7.1_mvapich2-2.2b/WRFV3/run/*
```

2) Create a run folder in the same root directory as `$case1_data`, referred as `$case1_run`
3) Link files from `WRFV3/run` into  `$case1_run`

``` bash
for l in $(ls /lustre/ARU/aru/opt/WRF3.7.1_mvapich2-2.2b/WRFV3/run/*); 
do
ln -s $l;
done
```

3) Remove some files ( sunce we're using the files provided by benchmark)

```bash
rm namelist*
```

4) Link files from `$case1_data` to `$case1_run`

```bash 
for l in $(ls ../conus12km_data_v3/*); do ln -s $l; done
```

5) Replace `namelist.input` symlink with the one in `$case1_data`

```bash
rm namelist.input
cp $case1_data/namelist.input .
```

6) Inside directory `$case1_run`, run `wrf.exe`, also keeping the standard output and error

```bash
$MPI="mpirun -np 16"
$MPI wrf.exe |& tee -a wrf.log
```

### CASE 2