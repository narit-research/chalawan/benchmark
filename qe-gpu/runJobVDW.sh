# !/bin/bash

# load required module files
module purge; module load pgi/18.4_cuda9.1 mvapich_pgi/2.2 intel/2015

export PATH=$PATH:/home/ittipat/benchmark/qe/bin

export CUDA_VISIBLE_DEVICES=0,2,3

mpirun -np 3 pw.x < ./szaf25_c03_vdw-df-cx_444_90_v180.scf.initial > szaf25_c03_vdw-df-cx_444_90_v180.scf.initial.out
