# !/bin/bash

export PATH=/opt/mvapich2-2.2/bin:/opt/pgi/linux86-64/2018/cuda/9.1/nvvm/bin:/opt/pgi/linux86-64/2018/cuda/9.1/bin:/opt/pgi/linux86-64/18.4/bin:/opt/intel/composer_xe_2015.1.133/mpirt/bin/intel64:/opt/intel/composer_xe_2015.1.133/debugger/python/intel64/bin:/opt/intel/composer_xe_2015.1.133/debugger/gdb/intel64/bin:/opt/intel/composer_xe_2015.1.133/bin/intel64:/usr/lib64/qt-3.3/bin:/usr/local/Modules/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/ibutils/bin:/home/ittipat/.local/bin:/home/ittipat/bin

export LD_LIBRARY_PATH=/opt/mvapich2-2.2/lib:/opt/pgi/linux86-64/2018/cuda/9.1/nvvm/lib64:/opt/pgi/linux86-64/2018/cuda/9.1/lib64:/opt/pgi/linux86-64/18.4/lib:/opt/intel/composer_xe_2015.1.133/mpirt/lib/intel64:/opt/intel/composer_xe_2015.1.133/ipp/lib/intel64:/opt/intel/composer_xe_2015.1.133/compiler/lib/intel64:/opt/intel/composer_xe_2015.1.133/mkl/lib/intel64:/opt/intel/composer_xe_2015.1.133/tbb/lib/intel64/gcc4.4

export MPI_ROOT=/opt/mvapich2-2.2

mpirun -np 4 -bind-to none ./runHelloByCore.sh
