# !/bin/bash

# load required module files
module purge; module load pgi/18.4_cuda9.1 mvapich_pgi/2.2 intel/2015

export PATH=$PATH:/home/ittipat/benchmark/qe/bin

export CUDA_VISIBLE_DEVICES=1,2,3

mpirun -np 3 pw.x < ./szaf25_c03_pbesol-scalrela_444_90_v180.scf.in2 > ./szaf25_c03_pbesol-scalrela_444_90_v180.scf_Test.out
