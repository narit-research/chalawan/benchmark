# !/bin/bash

# Total CPU cores / Total GPUs
CPU_CORES_PER_RANK=6

export MPI_ROOT=/opt/mvapich2-2.2
export OMP_NUM_THREADS=$CPU_CORES_PER_RANK
export MV2_ENABLE_AFFINITY=0

lrank=$OMPI_COMM_WORLD_LOCAL_RANK
echo $lrank
#./mpi_hello_world
case ${lrank} in
[0])
echo "case 0, lrank: $lrank:"; numactl --physcpubind=0,4,8,12 --membind=0 ./mpi_hello_world
;;
[1])
echo "case 1, lrank: $lrank:"; numactl --physcpubind=1,5,9,13 --membind=0 ./mpi_hello_world;;
[2])
echo "case 2, lrank: $lrank:"; numactl --physcpubind=2,6,10,14 --membind=0 ./mpi_hello_world;;
[3])
echo "case 0, lrank: $lrank:"; numactl --physcpubind=3,7,11,15 --membind=0 ./mpi_hello_world;;
esac
