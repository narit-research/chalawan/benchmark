# !/bin/bash

# Number of CPU cores
# Total CPU cores / Total GPUs (not counting hyperthreading)
CPU_CORES_PER_RANK=6
export MV2_ENABLE_AFFINITY=0

export MPI_ROOT=/opt/mvapich2-2.2

export OMP_NUM_THREADS=$CPU_CORES_PER_RANK

export CUDA_DEVICE_MAX_CONNECTIONS=12
export CUDA_COPY_SPLIT_THRESHOLD_MB=1

export GPU_DGEMM_SPLIT=1.0
export TRSM_CUTOFF=1000000

export TEST_SYSTEM_PARAMS=1
APP=/home/ittipat/benchmark/qe/bin/pw.x

#echo "CPU_CORES_PER_RANK=$CPU_CORES_PER_RANK"

#ldd $APP

lrank=$OMPI_COMM_WORLD_LOCAL_RANK

case ${lrank} in
[0])
#ldd $APP
export CUDA_VISIBLE_DEVICES=0; numactl --physcpubind=0,1,2,3,4 --membind=0 $APP -ni 1 -nt 1 -nk 1 < /home/ittipat/benchmark/qe/szaf25_c03_benchmark/szaf25_c03_pbesol-scalrela_444_90_v180.scf.in
  ;;
[1])
export CUDA_VISIBLE_DEVICES=1; numactl --physcpubind=5,6,7,8 --membind=0 $APP -ni 1 -nt 1 -nk 1 < /home/ittipat/benchmark/qe/szaf25_c03_benchmark/szaf25_c03_pbesol-scalrela_444_90_v180.scf.in
  ;;
[2])
export CUDA_VISIBLE_DEVICES=2; numactl --physcpubind=9,10,11,12 --membind=8 $APP -ni 1 -nt 1 -nk 1 < /home/ittipat/benchmark/qe/szaf25_c03_benchmark/szaf25_c03_pbesol-scalrela_444_90_v180.scf.in
  ;;
[3])
export CUDA_VISIBLE_DEVICES=3; numactl --physcpubind=13,14,15,16 --membind=8 $APP -ni 1 -nt 1 -nk 1< /home/ittipat/benchmark/qe/szaf25_c03_benchmark/szaf25_c03_pbesol-scalrela_444_90_v180.scf.in
  ;;
esac
