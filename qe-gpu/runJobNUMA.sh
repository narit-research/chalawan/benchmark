# !/bin/bash

# load required module files
module purge; module load pgi/18.4_cuda9.1 mvapich_pgi/2.2 intel/2015

export PATH=$PATH:/home/ittipat/benchmark/qe/bin

mpirun -np 4 numactl --localalloc --physcpubind=1,3,5,7 --membind=0 pw.x < ./szaf25_c03_pbesol-scalrela_444_90_v180.scf.in > ./szaf25_c03_pbesol-scalrela_444_90_v180.scf.out
