# !/bin/bash

# load required module files
module purge; module load pgi/18.4_cuda9.1 mvapich_pgi/2.2 intel/2015

export PATH=$PATH:/home/ittipat/benchmark/qe/bin

mpirun -np 2 pw.x < ./pw.in > ./pw_2gpu2.out
