#Full dspsr test on PSRGPU2 TX_0
echo "psrgpu2 dspsr on TitanX"
./benchTX_0.csh -freq=1400 -bw=8 -nchan=64 -gpu=0
cp f1*.* TX_0/
./benchTX_0.csh -freq=1400 -bw=8 -nchan=32 -gpu=0
cp f1*.* TX_0/
./benchTX_0.csh -freq=1400 -bw=8 -nchan=16 -gpu=0
cp f1*.* TX_0/
./benchTX_0.csh -freq=1400 -bw=8 -nchan=8 -gpu=0
cp f1*.* TX_0/
echo "benchmark complete" 
#
